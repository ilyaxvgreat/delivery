package com.khomchenko.delivery.repositories;

import com.khomchenko.delivery.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findRoleByAuthority(String roleName);

}
