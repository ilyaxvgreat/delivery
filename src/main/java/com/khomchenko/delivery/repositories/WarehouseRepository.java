package com.khomchenko.delivery.repositories;

import com.khomchenko.delivery.model.Warehouse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WarehouseRepository extends JpaRepository<Warehouse, Long> {
}
