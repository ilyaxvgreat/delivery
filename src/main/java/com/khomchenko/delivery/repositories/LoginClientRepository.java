package com.khomchenko.delivery.repositories;

import com.khomchenko.delivery.model.LoginClient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LoginClientRepository extends JpaRepository<LoginClient, Long> {

    Optional<LoginClient> findLoginClientByMobilePhone(String mobilePhone);

}
