package com.khomchenko.delivery.repositories;

import com.khomchenko.delivery.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {

    Optional<Client> findClientByMobilePhone(String mobilePhone);

    Optional<Client> findClientById(Long clientId);

}
