package com.khomchenko.delivery.repositories;

import com.khomchenko.delivery.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
