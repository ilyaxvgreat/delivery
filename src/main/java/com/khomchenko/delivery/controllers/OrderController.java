package com.khomchenko.delivery.controllers;

import com.khomchenko.delivery.exceptions.OrderException;
import com.khomchenko.delivery.model.Order;
import com.khomchenko.delivery.services.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private final OrderService orderService;

    public OrderController(final OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    public Order createNewOrder(@RequestBody final Order order) {
        return orderService.createOrder(order);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private void orderExceptionHandler(final OrderException exception) {
    }
}
