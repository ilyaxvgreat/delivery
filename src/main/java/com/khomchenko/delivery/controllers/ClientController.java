package com.khomchenko.delivery.controllers;

import com.khomchenko.delivery.exceptions.ClientMobilePhoneIsPresentException;
import com.khomchenko.delivery.exceptions.ClientNotFoundException;
import com.khomchenko.delivery.model.Client;
import com.khomchenko.delivery.services.ClientService;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class ClientController {

    private final ClientService clientService;

    public ClientController(final ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/clients/{clientId}")
    public Client getClientById(@PathVariable final Long clientId) {
        return clientService.getClientById(clientId);
    }

    @ExceptionHandler
    @ResponseStatus(BAD_REQUEST)
    private void clientMobilePhoneIsPresentHandler(final ClientMobilePhoneIsPresentException exception) {
    }

    @GetMapping("/mobiles/{mobileNumber}")
    public Client getClientByMobileNumber(@PathVariable final String mobileNumber) {
        return clientService.getClientByMobileNumber(mobileNumber);
    }

    @ExceptionHandler
    //выносить отдельно  rest exception advice
    @ResponseStatus(NOT_FOUND)
    private void clientNotFoundHandler(final ClientNotFoundException exception) {
    }
}
