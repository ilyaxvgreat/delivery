package com.khomchenko.delivery.controllers;

import com.khomchenko.delivery.model.DeliveryInfo;
import com.khomchenko.delivery.services.DeliveryInfoService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeliveryInfoController {

    private final DeliveryInfoService deliveryInfoService;

    public DeliveryInfoController(final DeliveryInfoService deliveryInfoService) {
        this.deliveryInfoService = deliveryInfoService;
    }

    @PostMapping("/deliveries-info")
    public DeliveryInfo createNewDeliveryInfo(@RequestBody final DeliveryInfo deliveryInfo) {
        return deliveryInfoService.createNewDeliveryInfo(deliveryInfo);
    }

    @PutMapping("/registration-time/{id}")
    public DeliveryInfo updateRegistrationDate(@PathVariable final Long id) {
        return deliveryInfoService.updateRegistrationDate(id);
    }
}
