package com.khomchenko.delivery.controllers;

import com.khomchenko.delivery.exceptions.ClientMobilePhoneIsPresentException;
import com.khomchenko.delivery.model.Client;
import com.khomchenko.delivery.services.RegistrationService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationController {

    private final RegistrationService registerNewClient;

    public RegistrationController(RegistrationService registerNewClient) {
        this.registerNewClient = registerNewClient;
    }

    @PostMapping("/clients")
    public Client registerNewClient(@RequestBody final Client newClient) {
        return registerNewClient.registerNewClient(newClient);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void clientIsPresentHandler(final ClientMobilePhoneIsPresentException exception) {
    }
}
