package com.khomchenko.delivery.services;

import com.khomchenko.delivery.exceptions.ClientNotFoundException;
import com.khomchenko.delivery.repositories.LoginClientRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class LoginClientService implements UserDetailsService {

    private final LoginClientRepository loginClientRepository;

    public LoginClientService(LoginClientRepository loginClientRepository) {
        this.loginClientRepository = loginClientRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return loginClientRepository.findLoginClientByMobilePhone(username)
                .orElseThrow(ClientNotFoundException::new);
    }
}
