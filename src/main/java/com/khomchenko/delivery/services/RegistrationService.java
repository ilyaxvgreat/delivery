package com.khomchenko.delivery.services;

import com.khomchenko.delivery.exceptions.ClientMobilePhoneIsPresentException;
import com.khomchenko.delivery.model.Client;
import com.khomchenko.delivery.repositories.ClientRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

    private final ClientRepository clientRepository;
    private final PasswordEncoder passwordEncoder;

    public RegistrationService(final ClientRepository clientRepository,
                               final PasswordEncoder passwordEncoder) {
        this.clientRepository = clientRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public Client registerNewClient(final Client newClient) {
        clientRepository.findClientByMobilePhone(newClient.getMobilePhone())
                .ifPresent(client -> {
                    throw new ClientMobilePhoneIsPresentException();
                });
        newClient.setPassword(passwordEncoder.encode(newClient.getPassword()));
        return clientRepository.save(newClient);
    }
}
