package com.khomchenko.delivery.services;

import com.khomchenko.delivery.exceptions.WarehouseNotFoundException;
import com.khomchenko.delivery.model.Category;
import com.khomchenko.delivery.model.Warehouse;
import com.khomchenko.delivery.repositories.WarehouseRepository;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class WarehouseService {

    private final WarehouseRepository warehouseRepository;

    public WarehouseService(WarehouseRepository warehouseRepository) {
        this.warehouseRepository = warehouseRepository;
    }

    public Set<Category> findCategoriesByWarehouseId(Long fromWarehouseId) {
        return findWarehouseById(fromWarehouseId).getCategories();
    }

    public Warehouse findWarehouseById(Long fromWarehouseId) {
        return warehouseRepository.findById(fromWarehouseId)
                .orElseThrow(WarehouseNotFoundException::new);
    }
}
