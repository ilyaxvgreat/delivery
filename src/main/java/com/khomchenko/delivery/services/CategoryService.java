package com.khomchenko.delivery.services;

import com.khomchenko.delivery.model.Category;
import com.khomchenko.delivery.repositories.CategoryRepository;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryService(final CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category getCategoryByName(final String name){
        return categoryRepository.findCategoryByCategoryName(name);
    }
}
