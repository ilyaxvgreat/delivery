package com.khomchenko.delivery.services;

import com.khomchenko.delivery.exceptions.InconsistencyOfCategoryDepartureWarehouse;
import com.khomchenko.delivery.exceptions.InconsistencyOfCategoryReceivingWarehouse;
import com.khomchenko.delivery.exceptions.OrderNotFoundException;
import com.khomchenko.delivery.exceptions.OrderWeightExceededException;
import com.khomchenko.delivery.model.Order;
import com.khomchenko.delivery.model.Warehouse;
import com.khomchenko.delivery.repositories.OrderRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final WarehouseService warehouseService;

    public OrderService(final OrderRepository orderRepository, final WarehouseService warehouseService) {
        this.orderRepository = orderRepository;
        this.warehouseService = warehouseService;
    }

    public Order createOrder(final Order order) {
        warehouseService.findCategoriesByWarehouseId(order.getFromWarehouseId())
                .stream()
                .filter(category -> category.getCategoryName()
                        .equals(order.getDeliveryInfo().getCategory().getCategoryName()))
                .findAny()
                .orElseThrow(InconsistencyOfCategoryDepartureWarehouse::new);

        warehouseService.findCategoriesByWarehouseId(order.getToWarehouseId())
                .stream()
                .filter(category -> category.getCategoryName()
                        .equals(order.getDeliveryInfo().getCategory().getCategoryName()))
                .findAny()
                .orElseThrow(InconsistencyOfCategoryReceivingWarehouse::new);

        checkOrderWeightLessWarehouseMaxWeight(order);
        return orderRepository.save(order);
    }

    public void checkOrderWeightLessWarehouseMaxWeight(final Order order) {
        final Warehouse fromWarehouse = warehouseService.findWarehouseById(order.getFromWarehouseId());
        if (order.getWeight() > fromWarehouse.getMaxOrderWeight()) {
            throw new OrderWeightExceededException();
        }
        final Warehouse toWarehouse = warehouseService.findWarehouseById(order.getToWarehouseId());
        if (order.getWeight() > toWarehouse.getMaxOrderWeight()) {
            throw new OrderWeightExceededException();
        }
    }

    public Order updateOrder(Long orderId, Order order){
        Order orderFromDB = orderRepository.findById(orderId)
                .orElseThrow(OrderNotFoundException::new);
        orderFromDB.setFromClientId(order.getFromClientId());
        orderFromDB.setToClientId(order.getToClientId());
        orderFromDB.setFromWarehouseId(order.getFromWarehouseId());
        orderFromDB.setFromWarehouseId(order.getToWarehouseId());
        return orderRepository.save(orderFromDB);
    }
}
