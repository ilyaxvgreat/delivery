package com.khomchenko.delivery.services;

import com.khomchenko.delivery.exceptions.ClientNotFoundException;
import com.khomchenko.delivery.model.Client;
import com.khomchenko.delivery.repositories.ClientRepository;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

    private final ClientRepository clientRepository;

    public ClientService(final ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public Client getClientById(final Long id) {
        return clientRepository.findClientById(id)
                .orElseThrow(ClientNotFoundException::new);
    }

    public Client getClientByMobileNumber(final String mobilePhone) {
        return clientRepository.findClientByMobilePhone(mobilePhone)
                .orElseThrow(ClientNotFoundException::new);
    }

}
