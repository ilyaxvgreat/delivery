package com.khomchenko.delivery.services;

import com.khomchenko.delivery.exceptions.DeliveryInfoNotFoundException;
import com.khomchenko.delivery.model.DeliveryInfo;
import com.khomchenko.delivery.repositories.DeliveryInfoRepository;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Timestamp;

import static org.apache.tomcat.jni.Time.now;

@Service
public class DeliveryInfoService {

    private final DeliveryInfoRepository deliveryInfoRepository;

    public DeliveryInfoService(final DeliveryInfoRepository deliveryInfoRepository) {
        this.deliveryInfoRepository = deliveryInfoRepository;
    }

    public DeliveryInfo createNewDeliveryInfo(final DeliveryInfo deliveryInfo) {
        deliveryInfo.setCreationDate(new Timestamp(System.currentTimeMillis()));
        return deliveryInfoRepository.save(deliveryInfo);
    }

    public DeliveryInfo updateRegistrationDate(final Long deliveryInfoId) {
        final DeliveryInfo deliveryInfo = deliveryInfoRepository.findById(deliveryInfoId)
                .orElseThrow(DeliveryInfoNotFoundException::new);
        deliveryInfo.setRegistrationDate(new Timestamp(System.currentTimeMillis()));
        return deliveryInfoRepository.save(deliveryInfo);
    }

}
