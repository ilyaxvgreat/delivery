package com.khomchenko.delivery.model;

import lombok.Getter;

@Getter
public enum  CategoryName {
    DOCUMENTS("DOCUMENTS"),
    STAFF("STAFF"),
    OTHER("OTHER");

    private final String name;

    CategoryName(String name) {
        this.name = name;
    }
}
