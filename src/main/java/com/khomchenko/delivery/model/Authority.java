package com.khomchenko.delivery.model;

import lombok.Getter;

@Getter
public enum Authority {

    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_MANAGER("ROLE_MANAGER"),
    ROLE_USER("ROLE_USER");

    private final String name;

    Authority(String name) {
        this.name = name;
    }
}
