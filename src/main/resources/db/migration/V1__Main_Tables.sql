create table if not exists clients
(
    id                         bigserial          not null,
    name                       varchar(30)        not null,
    last_name                  varchar(30)        not null,
    mobile_phone               varchar(13) unique not null,
    password                   varchar(100)       not null,
    gender                     varchar(1)         not null,
    country                    varchar(30)        not null,
    state                      varchar(30)        not null,
    city                       varchar(30)        not null,
    address                    varchar(100)       not null,
    is_account_non_expired     boolean            not null default true,
    is_account_non_locked      boolean            not null default true,
    is_credentials_non_expired boolean            not null default true,
    is_enabled                 boolean            not null default true,

    primary key (id)
);

create table if not exists positions
(
    id            bigserial    not null,
    position_name varchar(100) not null,

    primary key (id)
);

create table if not exists employees
(
    id          bigint       not null,
    name        varchar(100) not null,
    gender      varchar(1),
    last_name   varchar(100) not null,
    position_id bigint       not null,

    primary key (id),

    foreign key (position_id) references positions (id)
);

create table if not exists warehouses
(
    id               bigserial    not null,
    country          varchar(30)  not null,
    state            varchar(30)  not null,
    city             varchar(30)  not null,
    address          varchar(100) not null,
    number           bigint       not null unique,
    max_order_weight int          not null,
    administrator_id bigint,

    primary key (id),

    foreign key (administrator_id) references employees (id)
);

create table if not exists warehouses_employees
(
    id           bigserial not null,
    warehouse_id bigint    not null,
    employees_id bigint    not null,

    primary key (id),

    foreign key (warehouse_id) references warehouses (id),
    foreign key (employees_id) references employees (id)
);

create table if not exists categories
(
    id       bigserial not null,
    category varchar(100),

    primary key (id)
);

create table if not exists deliveries_info
(
    id                bigserial not null,
    total_price       int       not null,
    category_id       int,
    creation_date     date,
    registration_date date,
    start_date        date,
    delivery_date     date,

    primary key (id),

    foreign key (category_id) references categories (id)
);

create table if not exists orders
(
    id                   bigserial    not null,
    from_client_id       bigint,
    to_client_id         bigint,
    description          varchar(255) not null,
    weight               float        not null,
    volume               float        not null,
    from_warehouse_id    bigint,
    to_warehouse_id      bigint,
    current_warehouse_id bigint,
    delivery_info_id     bigint,

    primary key (id),

    foreign key (delivery_info_id) references deliveries_info (id) on DELETE cascade ,
    foreign key (from_client_id) references clients (id),
    foreign key (to_client_id) references clients (id),
    foreign key (from_warehouse_id) references warehouses (id),
    foreign key (to_warehouse_id) references warehouses (id),
    foreign key (current_warehouse_id) references warehouses (id)
);

create table if not exists status_type
(
    id   bigserial not null,
    type varchar(100),

    primary key (id)
);

create table if not exists status_types_orders
(
    id             bigserial not null,
    last_update    date,
    order_id       bigint,
    status_type_id int,

    primary key (id),

    foreign key (order_id) references orders (id),
    foreign key (status_type_id) references status_type (id)
);

create table if not exists order_parameters
(
    id       bigserial not null,
    order_id bigint,
    height   int       not null,
    weight   int       not null,
    depth    int       not null,

    primary key (id),

    foreign key (order_id) references orders (id)

);

create table if not exists warehouses_categories
(
    id           bigserial not null,
    warehouse_id bigint,
    category_id  bigint,

    primary key (id),

    foreign key (warehouse_id) references warehouses (id),
    foreign key (category_id) references categories (id)
);

create table if not exists warehouse_max_order_parameters
(
    id           bigserial not null,
    warehouse_id bigint,
    height       int       not null,
    weight       int       not null,
    depth        int       not null,

    primary key (id),

    foreign key (warehouse_id) references warehouses (id)
);

create table if not exists roles
(
    id        bigint,
    role_name varchar,
    primary key (id)
);

create table if not exists roles_clients
(
    client_id bigint,
    foreign key (client_id) references clients (id),
    role_id   bigint,
    foreign key (role_id) references roles (id)
);

insert into clients (name, last_name, mobile_phone, password, gender, country, state, city, address,
                     is_account_non_expired, is_account_non_locked, is_credentials_non_expired, is_enabled)
values ('a', 'a', 'a', '$2y$12$HlBYS2CO.e6.J1621dqvE.pj3rZhxOlo8Qa3M3nP5/WTzlV17bP/u', 'a', 'a', 'a', 'a', 'a', true,
        true, true, true)
