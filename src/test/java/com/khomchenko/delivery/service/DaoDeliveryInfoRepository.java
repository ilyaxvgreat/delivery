package com.khomchenko.delivery.service;

import com.khomchenko.delivery.model.DeliveryInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DaoDeliveryInfoRepository extends JpaRepository<DeliveryInfo, Long> {
}
