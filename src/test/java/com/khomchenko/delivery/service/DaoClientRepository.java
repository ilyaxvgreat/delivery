package com.khomchenko.delivery.service;

import com.khomchenko.delivery.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DaoClientRepository extends JpaRepository<Client, Long> {
}
