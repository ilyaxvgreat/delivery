package com.khomchenko.delivery.service;

import com.khomchenko.delivery.model.Warehouse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DaoWarehouseRepository extends JpaRepository<Warehouse, Long> {
}
