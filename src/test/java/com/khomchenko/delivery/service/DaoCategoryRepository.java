package com.khomchenko.delivery.service;

import com.khomchenko.delivery.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DaoCategoryRepository extends JpaRepository<Category, Long> {
}
