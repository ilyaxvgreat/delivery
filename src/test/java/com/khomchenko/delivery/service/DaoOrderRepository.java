package com.khomchenko.delivery.service;

import com.khomchenko.delivery.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DaoOrderRepository extends JpaRepository<Order,Long> {
}
