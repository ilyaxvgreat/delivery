package com.khomchenko.delivery.controllers;

import com.google.gson.Gson;
import com.khomchenko.delivery.model.Client;
import com.khomchenko.delivery.service.DaoClientRepository;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.flywaydb.test.annotation.FlywayTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FlywayTest
@WithMockUser(username = "admin", password = "admin")
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureEmbeddedDatabase
class ClientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DaoClientRepository clientDaoClientRepository;

    private Client testClient;

    private final Gson gson = new Gson();

    @BeforeEach
    void beforeEach() {
        testClient = clientDaoClientRepository.save(Client.builder()
                .mobilePhone("0000000000")
                .name("test")
                .lastName("test")
                .password("test")
                .state("test")
                .gender("M")
                .country("test")
                .city("test")
                .address("test")
                .build());
    }

    @AfterEach
    void afterEach() {
        clientDaoClientRepository.deleteAll();
    }

    @Test
    public void clientIsPresent_getClientById_returnClient() throws Exception {
        ResultActions resultActions = mockMvc.perform(get("/clients/" + testClient.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Client client = gson.fromJson(
                resultActions.andReturn().getResponse().getContentAsString(),
                Client.class);

        assertEquals(client.getId(), testClient.getId());
    }

    @Test
    public void clientIsNotPresent_getClientById_throwException() throws Exception {
        mockMvc.perform(get("/clients/3")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());


    }

}
