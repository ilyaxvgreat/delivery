//package com.khomchenko.delivery.controllers;
//
//import com.google.gson.Gson;
//import com.khomchenko.delivery.service.DaoClientRepository;
//import com.khomchenko.delivery.model.Client;
//import org.flywaydb.test.annotation.FlywayTest;
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestInstance;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@SpringBootTest
//@AutoConfigureMockMvc
//@FlywayTest
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
//class RegistrationControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private DaoClientRepository clientDaoClientRepository;
//
//    private final Gson gson = new Gson();
//    private final String presentMobilePhone = "00000000000";
//    private final String newMobilePhone = "11111111111";
//    private Client testClient;
//
//    @BeforeEach
//    void setUp() {
//        testClient = clientDaoClientRepository.save(Client.builder()
//                .mobilePhone(presentMobilePhone)
//                .name("test")
//                .lastName("test")
//                .password("test")
//                .state("test")
//                .gender("M")
//                .country("test")
//                .city("test")
//                .address("test")
//                .build());
//    }
//
//    @AfterEach
//    void afterEach() {
//        clientDaoClientRepository.delete(testClient);
//    }
//
//    @AfterAll
//    void afterAll() {
//        clientDaoClientRepository.deleteAll();
//    }
//
//    @Test
//    void clientMobilePhoneIsPresent_registerNewClient_returnBadRequest() throws Exception {
//        mockMvc.perform(post("/clients")
//                .content(gson.toJson(testClient))
//                .contentType(MediaType.APPLICATION_JSON)
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(
//                        status().isBadRequest()
//                );
//    }
//
//    @Test
//    void clientMobilePhoneIsNew_registerNewClient_returnNewClient() throws Exception {
//       testClient.setMobilePhone(newMobilePhone);
//
//        mockMvc.perform(post("/clients")
//                .content(gson.toJson(testClient))
//                .contentType(MediaType.APPLICATION_JSON)
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(
//                        status().isOk()
//                );
//    }
//}
