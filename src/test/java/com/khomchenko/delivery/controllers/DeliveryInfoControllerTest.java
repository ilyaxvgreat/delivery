//package com.khomchenko.delivery.controllers;
//
//import com.google.gson.Gson;
//import com.khomchenko.delivery.model.DeliveryInfo;
//import com.khomchenko.delivery.service.DaoDeliveryInfoRepository;
//import org.flywaydb.test.annotation.FlywayTest;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.ResultActions;
//
//import java.sql.Timestamp;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@FlywayTest
//@WithMockUser(username = "admin",password = "admin")
//@SpringBootTest
//@AutoConfigureMockMvc
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//class DeliveryInfoControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private DaoDeliveryInfoRepository daoDeliveryInfoRepository;
//
//    private final Gson gson = new Gson();
//
//    private DeliveryInfo testDeliveryInfo;
//
//    @BeforeEach
//    void setUp() {
//        DeliveryInfo deliveryInfo = new DeliveryInfo();
//        deliveryInfo.setTotalPrice(100);
//        deliveryInfo.setCreationDate(new Timestamp(System.currentTimeMillis()));
//        testDeliveryInfo = daoDeliveryInfoRepository.save(
//                deliveryInfo
//        );
//    }
//
//    @AfterEach
//    void afterEach() {
//        daoDeliveryInfoRepository.deleteAll();
//    }
//
//    @Test
//    void deliveryInfo_createNewDeliveryInfo_returnDeliveryInfoWithRegistrationDate() throws Exception {
//        DeliveryInfo testDeliveryInfo = DeliveryInfo.builder()
//                .totalPrice(100)
//                .build();
//
//        ResultActions resultActions = mockMvc.perform(
//                post("/deliveries-info")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .accept(MediaType.APPLICATION_JSON)
//                        .content(gson.toJson(testDeliveryInfo)))
//                .andExpect(status().isOk());
//
//        String contentAsString = resultActions.andReturn().getResponse().getContentAsString();
//
//        assertNotNull(gson.fromJson(contentAsString, DeliveryInfo.class).getCreationDate());
//    }
//
//    @Test
//    void updateRegistrationDate() throws Exception {
//        ResultActions perform = mockMvc.perform(
//                put("/registration-time/" + testDeliveryInfo.getId())
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .accept(MediaType.APPLICATION_JSON)
//
//        );
//
//        String contentAsString = perform.andReturn().getResponse().getContentAsString();
//
//        assertNotNull(gson.fromJson(contentAsString, DeliveryInfo.class).getRegistrationDate());
//    }
//}
