//package com.khomchenko.delivery.controllers;
//
//import com.google.gson.Gson;
//import com.khomchenko.delivery.model.Category;
//import com.khomchenko.delivery.model.CategoryName;
//import com.khomchenko.delivery.model.Client;
//import com.khomchenko.delivery.model.DeliveryInfo;
//import com.khomchenko.delivery.model.Order;
//import com.khomchenko.delivery.model.Warehouse;
//import com.khomchenko.delivery.service.DaoCategoryRepository;
//import com.khomchenko.delivery.service.DaoClientRepository;
//import com.khomchenko.delivery.service.DaoOrderRepository;
//import com.khomchenko.delivery.service.DaoWarehouseRepository;
//import com.khomchenko.delivery.services.CategoryService;
//import org.flywaydb.test.annotation.FlywayTest;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.web.servlet.MockMvc;
//
//import java.util.Set;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@FlywayTest
//@WithMockUser(username = "admin", password = "admin")
//@SpringBootTest
//@AutoConfigureMockMvc
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//class OrderControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private DaoClientRepository daoClientRepository;
//
//    @Autowired
//    private DaoCategoryRepository daoCategoryRepository;
//
//    @Autowired
//    private CategoryService categoryService;
//
//    @Autowired
//    private DaoOrderRepository orderRepository;
//
//    @Autowired
//    private DaoWarehouseRepository daoWarehouseRepository;
//
//    private Client fromClient;
//    private Client toClient;
//    private Warehouse fromWarehouse;
//    private Warehouse toWarehouse;
//    private Category category;
//    private final Gson gson = new Gson();
//
//    @BeforeEach
//    void setUp() {
//        category = daoCategoryRepository.save(Category.builder()
//                .categoryName("DOCUMENTS")
//                .build());
//        toClient = Client.builder()
//                .mobilePhone("111111111111")
//                .name("test")
//                .lastName("test")
//                .password("test")
//                .state("test")
//                .gender("M")
//                .country("test")
//                .city("test")
//                .address("test").build();
//        fromClient = daoClientRepository.save(
//                new Client(
//                        1L, "name", "lastname", "password", "00000", "M", "country", "state", "city", "address")
//        );
//        toClient = daoClientRepository.save(toClient);
//        fromWarehouse = daoWarehouseRepository.save(
//                fromWarehouse = Warehouse.builder()
//                        .address("test")
//                        .city("test")
//                        .categories(Set.of(category))
//                        .country("test")
//                        .maxOrderWeight(100)
//                        .state("test")
//                        .number(100L)
//                        .build()
//        );
//        toWarehouse = daoWarehouseRepository.save(
//                toWarehouse = Warehouse.builder()
//                        .address("test")
//                        .city("test")
//                        .categories(Set.of(category))
//                        .country("test")
//                        .maxOrderWeight(100)
//                        .state("test")
//                        .number(101L)
//                        .build()
//        );
//    }
//
//    @AfterEach
//    void afterEach() {
//        orderRepository.deleteAll();
//        daoClientRepository.deleteAll();
//        daoWarehouseRepository.deleteAll();
//    }
//
//    @Test
//    @WithMockUser(username = "admin", password = "admin")
//    public void orderIsCorrect_createOrder_returnOrder() throws Exception {
//        Order order = Order.builder()
//                .fromClientId(fromClient.getId())
//                .toClientId(toClient.getId())
//                .description("test")
//                .weight(100)
//                .fromWarehouseId(fromWarehouse.getId())
//                .toWarehouseId(toWarehouse.getId())
//                .volume(10)
//                .weight(10)
//                .deliveryInfo(
//                        DeliveryInfo.builder()
//                                .category(categoryService.getCategoryByName(CategoryName.DOCUMENTS.getName()))
//                                .build()
//                )
//                .build();
//
//        mockMvc.perform(post("/orders")
//                .content(gson.toJson(order))
//                .contentType(MediaType.APPLICATION_JSON)
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(
//                        status().isOk()
//                );
//    }
//
//}
