package com.khomchenko.delivery.services;

import com.khomchenko.delivery.model.DeliveryInfo;
import com.khomchenko.delivery.repositories.DeliveryInfoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.*;
import static org.mockito.MockitoAnnotations.initMocks;

class DeliveryInfoServiceTest {

    @InjectMocks
    private DeliveryInfoService deliveryInfoService;

    @Mock
    private DeliveryInfoRepository deliveryInfoRepository;

    @BeforeEach
    void setUp() {
       openMocks(this);
    }

    @Test
    void createNewDeliveryInfo() {
        DeliveryInfo testDeliveryInfo = new DeliveryInfo();
        given(deliveryInfoRepository.save(testDeliveryInfo)).willReturn(testDeliveryInfo);

        DeliveryInfo newDeliveryInfo = deliveryInfoService.createNewDeliveryInfo(testDeliveryInfo);

        assertNotNull(newDeliveryInfo.getCreationDate());
    }

    @Test
    void updateRegistrationDate() {
    }
}
