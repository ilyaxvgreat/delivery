package com.khomchenko.delivery.services;

import com.khomchenko.delivery.exceptions.ClientMobilePhoneIsPresentException;
import com.khomchenko.delivery.model.Client;
import com.khomchenko.delivery.repositories.ClientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;

class RegistrationServiceTest {

    @InjectMocks
    private RegistrationService registrationService;

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    private final String testMobilePhone = "000000000001";
    private final Client newTestClient = Client.builder().password("test").mobilePhone(testMobilePhone).build();

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void clientIsNotPresent_registerNewClient_returnClient() {
        given(clientRepository.findClientByMobilePhone(testMobilePhone))
                .willReturn(Optional.empty());
        given(clientRepository.save(newTestClient))
                .willReturn(newTestClient);
        given(passwordEncoder.encode(newTestClient.getPassword())).willReturn(anyString());

        Client expected = registrationService.registerNewClient(newTestClient);

        assertEquals(expected.getMobilePhone(), testMobilePhone);
    }

    @Test
    void clientMobilePhoneIsPresent_registerNewClient_throwException() {
        given(clientRepository.findClientByMobilePhone(testMobilePhone))
                .willReturn(Optional.of(newTestClient));

        assertThrows(ClientMobilePhoneIsPresentException.class,
                () -> registrationService.registerNewClient(newTestClient));
    }
}
