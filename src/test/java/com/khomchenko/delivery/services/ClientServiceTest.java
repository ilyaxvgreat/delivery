package com.khomchenko.delivery.services;

import com.khomchenko.delivery.exceptions.ClientNotFoundException;
import com.khomchenko.delivery.model.Client;
import com.khomchenko.delivery.repositories.ClientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;

class ClientServiceTest {

    @InjectMocks
    private ClientService clientService;

    @Mock
    private ClientRepository clientRepository;

    private final String testMobilePhone = "000000000001";
    private final Client newTestClient = Client.builder().mobilePhone(testMobilePhone).build();

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void clientIsPresent_getClientById_returnClient() {
        given(clientRepository.findClientById(1L))
                .willReturn(Optional.of(Client.builder().id(1L).build()));

        Client expected = clientService.getClientById(1L);

        assertEquals(expected.getId(), 1L);
    }

    @Test
    void clientIsNotPresent_getClientById_throwException() {
        given(clientRepository.findClientById(1L)).willThrow(ClientNotFoundException.class);

        assertThrows(ClientNotFoundException.class,
                () -> clientService.getClientById(1L));
    }

}
