package com.khomchenko.delivery.services;

import com.khomchenko.delivery.exceptions.InconsistencyOfCategoryDepartureWarehouse;
import com.khomchenko.delivery.exceptions.InconsistencyOfCategoryReceivingWarehouse;
import com.khomchenko.delivery.exceptions.OrderWeightExceededException;
import com.khomchenko.delivery.model.Category;
import com.khomchenko.delivery.model.DeliveryInfo;
import com.khomchenko.delivery.model.Order;
import com.khomchenko.delivery.model.Warehouse;
import com.khomchenko.delivery.repositories.OrderRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;

class OrderServiceTest {

    @InjectMocks
    private OrderService orderService;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private WarehouseService warehouseService;


    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void inconsistencyOfCategoryDepartureWarehouse_createOrder_throwException() {
        Category category = new Category(1L, "NOT_DOCUMENTS");
        DeliveryInfo deliveryInfo = new DeliveryInfo();
        deliveryInfo.setCategory(category);
        Order testOrder = Order.builder()
                .fromWarehouseId(1L)
                .deliveryInfo(deliveryInfo)
                .build();

        given(warehouseService.findCategoriesByWarehouseId(1L))
                .willReturn(Set.of(new Category(2L, "DOCUMENTS")));

        assertThrows(InconsistencyOfCategoryDepartureWarehouse.class,
                () -> orderService.createOrder(testOrder));
    }

    @Test
    void inconsistencyOfCategoryReceivingWarehouse_createOrder_throwException() {
        Category category = new Category(1L, "NOT_DOCUMENTS");
        DeliveryInfo deliveryInfo = new DeliveryInfo();
        deliveryInfo.setCategory(category);
        Order testOrder = Order.builder()
                .fromWarehouseId(1L)
                .toWarehouseId(2L)
                .deliveryInfo(deliveryInfo)
                .build();

        given(warehouseService.findCategoriesByWarehouseId(1L))
                .willReturn(Set.of(category));
        given(warehouseService.findCategoriesByWarehouseId(2L))
                .willReturn(Set.of(
                        new Category(1L, "DOCUMENTS")));

        assertThrows(InconsistencyOfCategoryReceivingWarehouse.class,
                () -> orderService.createOrder(testOrder));
    }

    @Test
    void orderWeightMoreThanStartWarehouseMaxWeight_isOrderWeightLessWarehouseMaxWeight_throwException() {
        Order order = Order.builder()
                .fromWarehouseId(1L)
                .toWarehouseId(2L)
                .weight(999)
                .build();
        Warehouse startWarehouse = Warehouse.builder().maxOrderWeight(9).build();

        given(orderRepository.findById(1L)).willReturn(Optional.of(order));
        given(warehouseService.findWarehouseById(1L)).willReturn((startWarehouse));

        Assertions.assertThrows(OrderWeightExceededException.class,
                () -> orderService.checkOrderWeightLessWarehouseMaxWeight(order));
    }

    @Test
    void orderWeightMoreThanFinishWarehouseMaxWeight_isOrderWeightLessWarehouseMaxWeight_throwException() {
        Order order = Order.builder()
                .fromWarehouseId(1L)
                .toWarehouseId(2L)
                .weight(999)
                .build();
        Warehouse startWarehouse = Warehouse.builder().maxOrderWeight(9999).build();
        Warehouse finishWarehouse = Warehouse.builder().maxOrderWeight(99).build();

        given(orderRepository.findById(1L)).willReturn(Optional.of(order));
        given(warehouseService.findWarehouseById(1L)).willReturn((startWarehouse));
        given(warehouseService.findWarehouseById(2L)).willReturn((finishWarehouse));

        Assertions.assertThrows(OrderWeightExceededException.class,
                () -> orderService.checkOrderWeightLessWarehouseMaxWeight(order));
    }
}
